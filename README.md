# Bank
## Description
This project programmatically describes a simple bank.

It has the following classes:
* User
* Person
* Company
* Account
* Address
* Date
* Representative
* Transaction
* NumberValidator

### User
User is the base class for Person and Company and constitutes a basic abstract for a bank client.
It has the following fields:
* email
* phone
* 2 addresses
* pointer to main account
* an array of pointers to savings accounts

### Person
Person extends User and has a field for PESEL.

### Company
Company extends User and has fields for a representative and NIP.

### Account
Account describes a bank account and has the following fields:
* type
* nrb
* creationDate
* transactionMap
* balance

### Transaction
Transaction describes a bank transaction and can be of two types - debit and credit. Credit is a transaction that adds money to the bank account and debit is a transaction that subtracts it.
