
#ifndef USER_H
#define USER_H

#include <string>
#include "account.h"
#include <memory>
#include <array>



struct Address{

    std::string street;
    std::string buildingNumber;
    std::string flatNumber;
    std::string postalCode;
    std::string city;
};


class User {

    protected:
        std::string email;
        std::string phone;
        Address address1;
        Address address2;
        std::unique_ptr<Account> mainAccount;
        std::array<std::unique_ptr<Account>, 5> savingsAccounts;

    public:
        std::string getEmail() const {return email;}
        std::string getPhone() const {return phone;}
        Address getPostAddress() const {return address2;}
        void setEmail(const std::string& email){this->email = email;}
        void setPhone(const std::string& phone){this->phone = phone;}
        void setPostAddress(const Address& address){this->address2 = address;}
        double getBalance() const;
        double getBalanceFromSavings(int index) const;
        double getSavingsBalance(int index) const;
        bool hasMainAccount() const {return (this->mainAccount.get()!=nullptr);}
        unsigned countSavingsAccounts() const;

        void assignMainAccount(const std::string& nrb, const Date& date);
        void addSavingsAccount(const std::string& nrb, const Date& date);
        void deleteMainAccount();
        void deleteSavingsAccount(int index);

        void makeTransfer(double value, Date date);
        void getMoney(double value, Date date);
        void saveMoney(int index, double value, Date date);
        void showMainTransactions();
        void showTransactionsFromSavings(int index);
        std::vector<Transaction> findTransactionsByDate(const Date& date) const;
};


class Person : public User{

    private:
        std::string name;
        std::string surname;
        std::string pesel;

    public:
        Person(const std::string& name, const std::string& surname, const std::string& pesel);
        std::string getName() const {return name;}
        std::string getSurname() const {return surname;}
        Address getHomeAddress() const {return address1;}
        std::string getPesel() const {return pesel;}
        void setName(const std::string& name){this->name = name;}
        void setSurname(const std::string& surname){this->surname = surname;}
        void setHomeAddress(const Address& address){this->address1 = address;}
        void setPesel(const std::string& pesel);
};


struct Representative{

    std::string name;
    std::string surname;
    std::string occupation; 
};


class Company : public User{

    private:
        std::string nip;
        Representative representative;

    public:
        Address getCompanyAddress() const {return address1;}
        std::string getNip() const {return nip;}
        Representative getRepresentative() const {return representative;}
        void setCompanyAddress(const Address& address){this->address1 = address;}
        void setNip(const std::string& nip);
        void setRepresentative(const Representative& representative){this->representative = representative;}
};


#endif 
