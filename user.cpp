
#include "user.h"
#include <iostream>
#include <algorithm>
#include <vector>

double User::getBalance() const {
    if (mainAccount.get() == nullptr){
        throw std::runtime_error("User has no main account.");
    }
    return mainAccount->getBalance();
}

double User::getBalanceFromSavings(int index) const{
    if (this->savingsAccounts.at(index).get()==nullptr){
        throw std::runtime_error("Cannot get balance from inexistent account.");
    }
    return this->savingsAccounts.at(index)->getBalance();
}


double User::getSavingsBalance(int index) const {
    if (index >= 0 && index < 5){
        throw std::domain_error("Index out of range.");
    }
    if (savingsAccounts[index].get() == nullptr){
        throw std::runtime_error("No account at this index.");
    }
    return savingsAccounts[index]->getBalance();
}

unsigned User::countSavingsAccounts() const{
    return std::count_if(this->savingsAccounts.cbegin(), this->savingsAccounts.cend(),
    [](const std::unique_ptr<Account>& ptr){
        return (ptr.get()!=nullptr);
    });
}

void User::assignMainAccount(const std::string& nrb, const Date& date){
    this->mainAccount=std::make_unique<Account>(Account::Type::MAIN, nrb, date);
}

void User::addSavingsAccount(const std::string& nrb, const Date& date){
    for (auto&& uniquePtr : savingsAccounts){
        if (uniquePtr.get()==nullptr){
            uniquePtr=std::make_unique<Account>(Account::Type::SAVINGS, nrb, date);
            return;
        }
    }
    throw std::runtime_error("No place for new savings account.");
}

void User::deleteMainAccount(){
    if (this->mainAccount.get()==nullptr)
        throw std::runtime_error("No main account to delete.");
    this->mainAccount.release();
}

void User::deleteSavingsAccount(int index){
    if (this->savingsAccounts.at(index).get()==nullptr)
        throw std::runtime_error("Account at index "+std::to_string(index)+" does not exist.");
    this->savingsAccounts.at(index).release();
}

void User::makeTransfer(double value, Date date){
    this->mainAccount->addTransaction(Transaction(Transaction::Type::DEBIT, date, value));
}

void User::getMoney(double value, Date date){
    this->mainAccount->addTransaction(Transaction(Transaction::Type::CREDIT, date, value));
}

void User::saveMoney(int index, double value, Date date){
    if (this->savingsAccounts.at(index).get()==nullptr){
        throw std::runtime_error("Cannot save money to inexistent account.");
    }
    this->savingsAccounts.at(index)->addTransaction(Transaction(Transaction::Type::CREDIT, date, value));
}

void User::showMainTransactions(){
    if (this->mainAccount.get()==nullptr){
        throw std::runtime_error("Cannot show transactions of inexistent account");
    }
    std::multimap<Date, Transaction> transactions {this->mainAccount->getTransactions()};
    for (auto&& transaction : transactions){
        std::cout << transaction.second << '\n';
    }
}

void User::showTransactionsFromSavings(int index){
    if (this->savingsAccounts.at(index).get()==nullptr){
        throw std::runtime_error("Cannot show transactions of inexistent account");
    }
    std::multimap<Date, Transaction> transactions {this->savingsAccounts.at(index)->getTransactions()};
    for (auto&& transaction : transactions){
        std::cout << transaction.second << '\n';
    }
}

std::vector<Transaction> User::findTransactionsByDate(const Date& date) const{
    return this->mainAccount->findTransactionsByDate(date);
}



Person::Person(const std::string& name, const std::string& surname, const std::string& pesel)
    : name(name), surname(surname) {
    setPesel(pesel);
}        

void Person::setPesel(const std::string& pesel){

    NumberValidator::validate(pesel, NumberValidator::Type::PESEL);
    this->pesel = pesel;
}


void Company::setNip(const std::string& nip){
    NumberValidator::validate(nip, NumberValidator::Type::NIP);
    this->nip = nip;
}
