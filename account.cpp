
#include "account.h"
#include <cctype>
#include <iostream>



void NumberValidator::validate(const std::string& number, Type type)
{
    if (number.size() != typeToSize(type)){
        throw std::runtime_error(typeToString(type)+" size is wrong. Current size: "+std::to_string(number.size())+" Maximum size: "+std::to_string(typeToSize(type)));
    }
    for (auto&& el : number){
        if (isalpha(el)){
            throw std::runtime_error(typeToString(type)+" contains alphabetic sign.");
        }
    }
}

void NumberValidator::validate(const Date& date)
{
    if (date.getMonth()<=0 || date.getMonth()>12)
        throw std::runtime_error("Invalid date format: month out of range.");
    std::map<int,int> monthDaysMap;
    int helpNum {1};
    for (int i = 0; i < 12; ++i){
        int days=(helpNum%2==0)?30:31;
        if (helpNum==2){
            days=28;
        }
        else if (helpNum==7){
            helpNum--;
        }
        monthDaysMap.insert(std::make_pair(i+1,days));
        ++helpNum;
    }
    if (monthDaysMap[date.getMonth()] < date.getDay())
        throw std::runtime_error(
            "Invalid date format: month "+std::to_string(date.getMonth())+
            " can have maximum "+std::to_string(monthDaysMap[date.getMonth()])+" days.");
    if (date.getDay() <= 0)
        throw std::runtime_error(
            "Invalid date format: day cannot be less or equal to zero.");   
}


unsigned NumberValidator::typeToSize(Type type)
{
    switch(type){
        case Type::NIP: return 10;
        case Type::PESEL: return 11;
        case Type::NRB: return 26;
        default: throw std::domain_error("Unknown type");
    }
}


std::string NumberValidator::typeToString(Type type){
    switch(type){
        case Type::NIP: return "NIP";
        case Type::PESEL: return "PESEL";
        case Type::NRB: return "NRB";
        default: throw std::domain_error("Unknown type");
    }
}


void Account::setNrb(const std::string& nrb){
    NumberValidator::validate(nrb, NumberValidator::Type::NRB);
    this->nrb = nrb;
}


void Account::addTransaction(const Transaction& transaction){
    transactionMap.insert(std::make_pair(transaction.getDate(), transaction));
    balance += transaction.getValue();
}

std::vector<Transaction> Account::findTransactionsByDate(const Date& date) const{
    std::vector<Transaction> transactionsByDate;
    auto itPair {this->transactionMap.equal_range(date)};
    for (auto it = itPair.first; it != itPair.second; ++it){
        transactionsByDate.push_back(it->second);
    }
    return transactionsByDate;
}

Date::Date(int day, int month, int year){
    setDate(day, month, year);
}


bool Date::operator<(const Date& other) const{
    if (this->year != other.year){
        return this->year < other.year;
    }
    else if (this->month != other.month){
        return this->month < other.month;
    }
    return this->day < other.day;
}


void Date::setDate(int day, int month, int year){
    
    setYear(year);
    setMonth(month);
    setDay(day);
    NumberValidator::validate(*this);
}

std::map<int, std::string> Date::monthsNamesMap(){
    return std::map<int, std::string> {
        std::make_pair(1, "January"),
        std::make_pair(2, "February"),
        std::make_pair(3, "March"),
        std::make_pair(4, "April"),
        std::make_pair(5, "May"),
        std::make_pair(6, "June"),
        std::make_pair(7, "July"),
        std::make_pair(8, "August"),
        std::make_pair(9, "September"),
        std::make_pair(10, "October"),
        std::make_pair(11, "November"),
        std::make_pair(12, "December")
    };
}

std::ostream& operator<<(std::ostream& os, const Date& date){
    return os << date.day << ' ' << Date::monthsNamesMap()[date.month] << ' ' << date.year;
}

std::ostream& operator<<(std::ostream& os, const Transaction& transaction){
    return os << transaction.getDate() << ',' << transaction.getValue();
}

