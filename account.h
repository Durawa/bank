
#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>
#include <map>
#include <vector>


class Date{

    private:
        int day;
        int month;
        int year;

    public:
        Date() = default;
        Date(const Date& other) = default;
        Date(Date&& other) noexcept = default;
        Date& operator=(const Date& other) = default;
        Date& operator=(Date&& other) noexcept = default;
        Date(int day, int month, int year);
        bool operator<(const Date& other) const;
        int getDay() const {return day;}
        int getMonth() const {return month;}
        int getYear() const {return year;}
        void setDate(int day, int month, int year);

        friend std::ostream& operator<<(std::ostream& os, const Date& date);

    private:
        void setDay(int day){this->day = day;}
        void setMonth(int month){this->month = month;}
        void setYear(int year){this->year = year;}     
        static std::map<int, std::string> monthsNamesMap();  
};


class NumberValidator{

    public:
        enum class Type {PESEL,NIP,NRB};
        static void validate(const std::string& number, Type type);
        static void validate(const Date& date);

    private:
        static unsigned typeToSize(Type type);
        static std::string typeToString(Type type);
};


class Transaction{

    public:
        enum class Type{DEBIT, CREDIT};

    private:
        Type type;
        Date date;
        double amount;

    public:
        Transaction(Type type, Date date, double amount)
         : type(type), date(date), amount(amount){}
        Type getType() const {return type;}
        Date getDate() const {return date;}
        double getValue() const {return (type == Type::CREDIT ? amount : -amount);}
        friend std::ostream& operator<<(std::ostream& os, const Transaction& transaction);
};


class Account{

    public:
        enum class Type{MAIN, SAVINGS};
    
    private:
        Type type;
        std::string nrb;
        Date creationDate;
        std::multimap<Date, Transaction> transactionMap;
        double balance;

    public:
        Account(Type type, const std::string& nrb, const Date& creationDate)
         : type(type), nrb(nrb), creationDate(creationDate){}
        void setNrb(const std::string& nrb);
        void addTransaction(const Transaction& transaction);
        std::vector<Transaction> findTransactionsByDate(const Date& date) const;
        double getBalance() const {return balance;}
        std::multimap<Date, Transaction> getTransactions() const {return this->transactionMap;}
};

#endif
