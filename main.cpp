
#include "user.h"
#include <iostream>
int main(){

    Person person("Jozek", "Wozek", "11111111111");
    person.assignMainAccount("11111111111111111111111111", Date(4,2,2021));
    std::cout<<person.getBalance()<<std::endl;
    person.makeTransfer(50, Date(4,2,2021));
    person.makeTransfer(450, Date(4,2,2021));
    person.getMoney(1000, Date(4,2,2021));
    person.makeTransfer(510, Date(4,2,2021));
    person.makeTransfer(60, Date(3,2,2021));
    person.makeTransfer(58, Date(3,2,2021));
    person.makeTransfer(523, Date(3,2,2021));
    person.getMoney(10000, Date(2, 2, 2021));
    person.makeTransfer(10, Date(2,2,2021));
    person.makeTransfer(450, Date(2,2,2021));
    person.makeTransfer(50, Date(2,2,2021));
    person.makeTransfer(70, Date(2,2,2021));
    person.showMainTransactions();

    std::cout<<person.getBalance()<<std::endl;

    std::cout<<std::endl<<std::endl;
    std::vector<Transaction> transactions {person.findTransactionsByDate(Date(2,2,2021))};
    for (auto&& transaction : transactions){
        std::cout << transaction << std::endl;
    }

    std::cout<<person.countSavingsAccounts()<<std::endl;

    person.addSavingsAccount("11111111111111111111111111", Date(4,2,2021));
    std::cout<<person.countSavingsAccounts()<<std::endl;
    person.saveMoney(0, 1000, Date(4,2,2021));

    person.showTransactionsFromSavings(0);
    return 0;
}
